using System.Collections.Generic;
using Exercise.BattleSimulation.Core.Domain;
using Exercise.BattleSimulation.Core.Domain.Service;
using Exercise.BattleSimulation.View;
using NSubstitute;
using NUnit.Framework;
using UnityEngine;

namespace Exercise.BattleSimulation.Core.Actions
{
	public class UpdateBattleCenterTest
	{
		BattleService battleService;
		UpdateBattleCenter updateBattleCenter;
		BattleCenterRepository battleCenterRepository;
		
		[SetUp]
		public void BeforeEach()
		{
			battleService = Substitute.For<BattleService>();
			battleCenterRepository = Substitute.For<BattleCenterRepository>();
			
			updateBattleCenter = new UpdateBattleCenter(battleService, battleCenterRepository);
		}
		
		[Test]
		public void Should_Update_Battle_Center()
		{
			var armyUnit1 = Substitute.For<ArmyUnit>();
			var armyUnit2 = Substitute.For<ArmyUnit>();
			var armyUnit3 = Substitute.For<ArmyUnit>();
			var armyUnit4 = Substitute.For<ArmyUnit>();
			armyUnit1.GetPosition().Returns(new Vector3(2, 2, 4));
			armyUnit2.GetPosition().Returns(new Vector3(23, 2, 6));
			armyUnit3.GetPosition().Returns(new Vector3(5, 2, 14));
			armyUnit4.GetPosition().Returns(new Vector3(7, 2, 2));
			var armyUnits = new List<ArmyUnit>() {armyUnit1, armyUnit2, armyUnit3, armyUnit4};
			GivenArmyUnits(armyUnits);

			var battleCenter = WhenCenterIsUpdated();

			ThenBattleCenterIsStored(battleCenter);
			ThenBattleCenterIsTheExpected(new Vector3(9.25f, 2, 6.5f), battleCenter);
		}

		void ThenBattleCenterIsTheExpected(Vector3 expectedCenter, Vector3 battleCenter)
		{
			Assert.IsTrue(Vector3TestUtils.AreEqual(expectedCenter,battleCenter));
		}

		void ThenBattleCenterIsStored(Vector3 battleCenter)
		{
			battleCenterRepository.Received(1).Put(battleCenter);
		}

		Vector3 WhenCenterIsUpdated()
		{
			return updateBattleCenter.Invoke();
		}

		void GivenArmyUnits(List<ArmyUnit> armyUnits)
		{
			battleService.GetAllUnits().Returns(armyUnits);
		}
	}
}