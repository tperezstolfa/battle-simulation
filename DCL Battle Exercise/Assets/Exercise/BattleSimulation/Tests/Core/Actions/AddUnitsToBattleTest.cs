using System.Collections.Generic;
using Exercise.Battle.Scripts;
using Exercise.BattleSimulation.Core.Domain.Service;
using Exercise.BattleSimulation.View;
using NSubstitute;
using NUnit.Framework;

namespace Exercise.BattleSimulation.Core.Actions
{
	public class AddUnitsToBattleTest
	{
		BattleService battleService;
		AddUnitsToBattle addUnitsToBattle;

		const int ArmyId = 1;
		
		[SetUp]
		public void BeforeEach()
		{
			battleService = Substitute.For<BattleService>();
			
			addUnitsToBattle = new AddUnitsToBattle(battleService);
		}

		[Test]
		public void Should_Add_Units_To_Battle()
		{
			var armyUnits = Substitute.For<List<ArmyUnit>>();	
			
			WhenUnitsAreAdded(ArmyId, armyUnits);

			ThenUnitsAreRegisteredInBattle(ArmyId, armyUnits);
		}

		void ThenUnitsAreRegisteredInBattle(int armyId, List<ArmyUnit> units)
		{
			battleService.Received(1).RegisterUnits(armyId, units);
		}

		void WhenUnitsAreAdded(int armyId, List<ArmyUnit> units)
		{
			addUnitsToBattle.Invoke(armyId, units);
		}
	}
}