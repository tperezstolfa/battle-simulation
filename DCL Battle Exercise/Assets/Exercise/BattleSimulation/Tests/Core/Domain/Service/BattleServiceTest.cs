using System.Collections.Generic;
using System.Linq;
using Exercise.Battle.Scripts;
using Exercise.BattleSimulation.View;
using NSubstitute;
using NUnit.Framework;

namespace Exercise.BattleSimulation.Core.Domain.Service
{
	public class BattleServiceTest
	{
		const int FirstArmyId = 0;
		const int SecondArmyId = 1;
		BattleService battleService;

		[SetUp]
		public void BeforeEach()
		{
			battleService = new BattleService();
		}

		[TestCase(FirstArmyId)]
		[TestCase(SecondArmyId)]
		public void Should_Register_Units_To_Battle_For_Any_Army_On_Empty_Battle(int armyId)
		{
			var armyUnits = GivenArmyUnits();
			
			WhenRegisteringUnits(armyId, armyUnits);

			ThenBattleHasUnitsForArmy(armyId, armyUnits);
		}
		
		[TestCase(FirstArmyId, SecondArmyId)]
		[TestCase(SecondArmyId, FirstArmyId)]
		public void Should_Register_Units_To_Battle_For_Any_Army_On_Already_Created_Battle(int armyId, int otherArmyId)
		{
			var previouslyRegisteredUnits = GivenAlreadyRegisteredArmyUnits(armyId);
			var previouslyRegisteredUnitsFotOtherArmy = GivenAlreadyRegisteredArmyUnits(otherArmyId);
			var newArmyUnits = GivenArmyUnits();
			
			WhenRegisteringUnits(armyId, newArmyUnits);

			ThenBattleHasUnitsForArmy(armyId, newArmyUnits);
			ThenBattleHasUnitsForArmy(armyId, previouslyRegisteredUnits);
			ThenBattleHasUnitsForArmy(otherArmyId, previouslyRegisteredUnitsFotOtherArmy);
		}

		[Test]
		public void Should_Return_Enemy_Units()
		{
			GivenAlreadyRegisteredArmyUnits(FirstArmyId);
			var enemyUnits = GivenAlreadyRegisteredArmyUnits(SecondArmyId);

			var retrievedUnits = WhenEnemyUnitsAreRequested(FirstArmyId);

			ThenUnitsAreRetrievedCorrectly(enemyUnits, retrievedUnits);
		}
		
		[Test]
		public void Should_Return_All_Units()
		{
			var unitsForFirstArmy = GivenAlreadyRegisteredArmyUnits(FirstArmyId);
			var unitsForSecondArmy = GivenAlreadyRegisteredArmyUnits(SecondArmyId);

			var retrievedUnits = WhenAllUnitsAreRequested();

			ThenUnitsAreRetrievedCorrectly(unitsForFirstArmy.Union(unitsForSecondArmy).ToList(), retrievedUnits);
		}

		List<ArmyUnit> WhenEnemyUnitsAreRequested(int armyId)
		{
			return battleService.GetEnemies(armyId);
		}
		
		List<ArmyUnit> WhenAllUnitsAreRequested()
		{
			return battleService.GetAllUnits();
		}

		void ThenUnitsAreRetrievedCorrectly(List<ArmyUnit> enemyUnits, List<ArmyUnit> retrievedUnits)
		{
			Assert.AreEqual(enemyUnits.Count, retrievedUnits.Count);
			retrievedUnits.ForEach(retrievedUnit => Assert.Contains(retrievedUnit, enemyUnits));
		}

		static List<ArmyUnit> GivenArmyUnits()
		{
			return new List<ArmyUnit>
				{Substitute.For<ArmyUnit>(), Substitute.For<ArmyUnit>(), Substitute.For<ArmyUnit>()};
		}
		
		List<ArmyUnit> GivenAlreadyRegisteredArmyUnits(int armyId)
		{
			var armyUnits = new List<ArmyUnit>
				{Substitute.For<ArmyUnit>(), Substitute.For<ArmyUnit>(), Substitute.For<ArmyUnit>()};
			battleService.RegisterUnits(armyId, armyUnits);
			return armyUnits;
		}

		void WhenRegisteringUnits(int armyId, List<ArmyUnit> units)
		{
			battleService.RegisterUnits(armyId, units);
		}

		void ThenBattleHasUnitsForArmy(int armyId, List<ArmyUnit> units)
		{
			units.ForEach(unit => Assert.Contains(unit, battleService.GetUnits(armyId)));
		}
	}
}