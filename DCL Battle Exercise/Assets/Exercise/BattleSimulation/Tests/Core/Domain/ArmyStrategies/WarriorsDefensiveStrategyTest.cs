using Exercise.BattleSimulation.Core.Actions;
using Exercise.BattleSimulation.Presentation.ArmyStrategies;
using Exercise.BattleSimulation.View;
using NSubstitute;
using NSubstitute.ReturnsExtensions;
using NUnit.Framework;
using UnityEngine;

namespace Exercise.BattleSimulation.Core.Domain.ArmyStrategies
{
	public class WarriorsDefensiveStrategyTest
	{
		WarriorsDefensiveStrategy battleStrategy;
		FindNearestEnemy findNearestEnemy;
		ArmyUnit currentUnit;
		const int ArmyId = 1;

		[SetUp]
		public void BeforeEach()
		{
			currentUnit = Substitute.For<ArmyUnit>();
			currentUnit.GetArmyId().Returns(ArmyId);
			findNearestEnemy = Substitute.For<FindNearestEnemy>();

			battleStrategy = new WarriorsDefensiveStrategy(findNearestEnemy);
		}

		[Test]
		public void Should_Not_Move_Nor_Attack_If_No_Enemy_Is_Found()
		{
			GivenThereIsNoEnemyNearby();

			WhenApplied();

			ThenUnitDoesNotMove();
			ThenUnitDoesNotAttack();
		}

		[Test]
		public void Should_Move_Away_From_Enemy_When_Cannot_Attack()
		{
			var nearbyEnemy = GivenAnEnemyNearby();
			GivenAnUnitPosition(currentUnit, new Vector3(-10, 4, 3));
			GivenAnUnitPosition(nearbyEnemy, new Vector3(10, 2, 5));
			GivenUnitCooldown(2);

			WhenApplied();

			ThenUnitMovesIgnoringY(new Vector3(-1f, 0f, -0.1f));
			ThenUnitDoesNotAttack();
		}

		[Test]
		public void Should_Move_Towards_And_Attack_Enemy_When_Can_Attack()
		{
			var nearbyEnemy = GivenAnEnemyNearby();
			GivenAnUnitPosition(currentUnit, new Vector3(-10, 4, 3));
			GivenAnUnitPosition(nearbyEnemy, new Vector3(10, 2, 5));
			GivenUnitCooldown(0);

			WhenApplied();

			ThenUnitMovesIgnoringY(new Vector3(1f, 0f, 0.1f));
			ThenUnitAttacks(nearbyEnemy);
		}

		void ThenUnitAttacks(ArmyUnit nearbyEnemy)
		{
			currentUnit.Received(1).Attack(nearbyEnemy);
		}

		void ThenUnitMovesIgnoringY(Vector3 direction)
		{
			currentUnit.Received(1).Move(Arg.Is<Vector3>(vector => Vector3TestUtils.AreEqual(vector, direction)));
		}

		void GivenAnUnitPosition(ArmyUnit armyUnit, Vector3 position)
		{
			armyUnit.GetPosition().Returns(position);
		}

		ArmyUnit GivenAnEnemyNearby()
		{
			var nearestEnemy = Substitute.For<ArmyUnit>();
			findNearestEnemy.Invoke(currentUnit).Returns(nearestEnemy);

			return nearestEnemy;
		}

		void GivenUnitCooldown(int cooldown)
		{
			currentUnit.GetCooldown().Returns(cooldown);
		}

		void GivenThereIsNoEnemyNearby()
		{
			findNearestEnemy.Invoke(currentUnit).ReturnsNull();
		}

		void WhenApplied()
		{
			battleStrategy.Apply(currentUnit);
		}

		void ThenUnitDoesNotMove()
		{
			currentUnit.DidNotReceive().Move(Arg.Any<Vector3>());
		}

		void ThenUnitDoesNotAttack()
		{
			currentUnit.DidNotReceive().Attack(Arg.Any<UnitBase>());
		}
	}
}