using UnityEngine;

namespace Exercise.BattleSimulation
{
	internal static class Vector3TestUtils
	{
		public static bool AreEqual(Vector3 expected, Vector3 actual)
		{
			return Vector3.SqrMagnitude(expected - actual) < 0.001;
		}
	}
}