using System;
using System.Collections;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using Exercise.BattleSimulation.Core.Domain.Service;
using Exercise.BattleSimulation.Presentation.ArmyStrategies;
using Exercise.BattleSimulation.View;
using NSubstitute;
using NUnit.Framework;
using UnityEngine.TestTools;

namespace Exercise.BattleSimulation.Presentation
{
	public class ArmyUnitPresenterTest
	{
		ArmyUnitPresenter presenter;
		ArmyUnit view;
		BattleService battleService;
		BasicRulesBehaviour basicRulesBehaviour;

		[SetUp]
		public void BeforeEach()
		{
			view = Substitute.For<ArmyUnit>();
			battleService = Substitute.For<BattleService>();
			basicRulesBehaviour = Substitute.For<BasicRulesBehaviour>();
			
			presenter = new ArmyUnitPresenter(view, battleService, basicRulesBehaviour);
		}

		[Test]
		public void Should_Initialize_Army_Unit()
		{
			var armyStrategy = Substitute.For<ArmyStrategy>();
			var armyId = 1;

			WhenPresents(armyStrategy, armyId);

			ThenArmyIdIsSet(armyId);
		}
		
		[Test]
		public void Should_Follow_Strategy_And_Basic_Rules_On_Update()
		{
			var armyStrategy = Substitute.For<ArmyStrategy>();
			var armyId = 1;

			WhenPresents(armyStrategy, armyId);
			view.OnUpdate += Raise.Event<Action>();

			ThenAppliesStrategy(armyStrategy);
			ThenAppliesBasicRules();
		}

		void WhenPresents(ArmyStrategy armyStrategy, int armyId)
		{
			presenter.Present(armyStrategy, armyId);
		}

		void ThenAppliesStrategy(ArmyStrategy armyStrategy)
		{
			armyStrategy.Received(1).Apply(view);
		}

		void ThenAppliesBasicRules()
		{
			basicRulesBehaviour.Received(1).Apply(view);
		}

		void ThenArmyIdIsSet(int armyId)
		{
			view.Received(1).SetArmyId(armyId);
		}
	}
}