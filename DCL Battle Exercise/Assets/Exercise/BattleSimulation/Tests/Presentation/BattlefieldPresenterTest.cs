using System;
using System.Collections.Generic;
using Exercise.Battle.Scripts;
using Exercise.BattleCreation.Core.Domain;
using Exercise.BattleSimulation.Core.Actions;
using Exercise.BattleSimulation.Core.Domain;
using Exercise.BattleSimulation.Core.Domain.Service;
using Exercise.BattleSimulation.Presentation.ArmyStrategies;
using Exercise.BattleSimulation.Presentation.Service;
using Exercise.BattleSimulation.View;
using NSubstitute;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.UIElements;

namespace Exercise.BattleSimulation.Presentation
{
	public class BattlefieldPresenterTest
	{
		BattlefieldView view;
		ArmySettingsRepository armySettingsRepository;
		BattlefieldPresenter presenter;
		AddUnitsToBattle addUnitsToBattle;
		ArmyUnitPresenterFactory armyUnitPresenterFactory;
		ArmyStrategyProvider armyStrategyProvider;
		UpdateBattleCenter updateBattleCenter;

		[SetUp]
		public void BeforeEach()
		{
			view = Substitute.For<BattlefieldView>();
			addUnitsToBattle = Substitute.For<AddUnitsToBattle>();
			armySettingsRepository = Substitute.For<ArmySettingsRepository>();
			armyUnitPresenterFactory = Substitute.For<ArmyUnitPresenterFactory>();
			view.InstantiateArchers(Arg.Any<int>(), Arg.Any<int>()).Returns(new List<ArmyUnit>());
			view.InstantiateWarriors(Arg.Any<int>(), Arg.Any<int>()).Returns(new List<ArmyUnit>());
			armyStrategyProvider = Substitute.For<ArmyStrategyProvider>();
			updateBattleCenter = Substitute.For<UpdateBattleCenter>();

			BattleService battleService = Substitute.For<BattleService>();
			presenter = new BattlefieldPresenter(view, armySettingsRepository, addUnitsToBattle, armyUnitPresenterFactory, battleService, armyStrategyProvider, updateBattleCenter);
		}

		[Test]
		public void Should_Instantiate_Archers_For_Configured_Armies_And_Present_Units()
		{
			const int army1Id = 0;
			const int army2Id = 1;
			var army1Settings = GivenAConfiguredArmy(army1Id);
			var army2Settings = GivenAConfiguredArmy(army2Id);
			var listOfArchersForArmy1 = GivenArchersWillBeCreated(army1Id, army1Settings.archerCount);
			var presentersForArmy1Units = GivenPresentersWillBeCreatedForUnits(listOfArchersForArmy1);
			var listOfArchersForArmy2 = GivenArchersWillBeCreated(army2Id, army2Settings.archerCount);
			var presentersForArmy2Units = GivenPresentersWillBeCreatedForUnits(listOfArchersForArmy2);
			var archer1sStrategy = GivenAnStrategyForArchers(army1Settings.armyStrategyName);
			var archers2Strategy = GivenAnStrategyForArchers(army1Settings.armyStrategyName);

			WhenPresents();

			ThenArmyArchersAreCreated(army1Id, army1Settings.archerCount);
			ThenArmyArchersAreCreated(army2Id, army2Settings.archerCount);
			addUnitsToBattle.Received(1).Invoke(army1Id, listOfArchersForArmy1);
			ThenUnitsArePresented(presentersForArmy1Units, archer1sStrategy, army1Id);
			addUnitsToBattle.Received(1).Invoke(army2Id, listOfArchersForArmy2);
			ThenUnitsArePresented(presentersForArmy2Units, archers2Strategy, army2Id);

		}

		[Test]
		public void Should_Instantiate_Warriors_For_Configured_Armies_And_Present_Units()
		{
			const int army1Id = 0;
			const int army2Id = 1;
			var army1Settings = GivenAConfiguredArmy(army1Id);
			var army2Settings = GivenAConfiguredArmy(army2Id);
			var listOfWarriorsForArmy1 = GivenWarriorsWillBeCreated(army1Id, army1Settings.warriorCount);
			var presentersForArmy1Units = GivenPresentersWillBeCreatedForUnits(listOfWarriorsForArmy1);
			var listOfWarriorsForArmy2 = GivenWarriorsWillBeCreated(army2Id, army2Settings.warriorCount);
			var presentersForArmy2Units = GivenPresentersWillBeCreatedForUnits(listOfWarriorsForArmy2);
			var warriors1Strategy = GivenAnStrategyForWarriors(army1Settings.armyStrategyName);
			var warriors2Strategy = GivenAnStrategyForWarriors(army1Settings.armyStrategyName);
			
			WhenPresents();

			ThenArmyWarriorsAreCreated(army1Id, army1Settings.warriorCount);
			ThenArmyWarriorsAreCreated(army2Id, army2Settings.warriorCount);
			addUnitsToBattle.Received(1).Invoke(army1Id, listOfWarriorsForArmy1);
			ThenUnitsArePresented(presentersForArmy1Units, warriors1Strategy, army1Id);
			addUnitsToBattle.Received(1).Invoke(army2Id, listOfWarriorsForArmy2);
			ThenUnitsArePresented(presentersForArmy2Units, warriors2Strategy, army2Id);
		}

		[Test]
		public void Should_Update_Battle_Center_And_Set_Camera_Position_On_Update()
		{
			var battleCenter = new Vector3(1, 1, 1);
			updateBattleCenter.Invoke().Returns(battleCenter);
			view.OnUpdate += Raise.Event<Action>();

			updateBattleCenter.Received(1).Invoke();
			view.Received(1).MoveCamera(battleCenter);
		}

		ArmyStrategy GivenAnStrategyForArchers(ArmyStrategyName armyStrategyName)
		{
			var armyStrategy = Substitute.For<ArmyStrategy>();
			armyStrategyProvider.ForArchers(armyStrategyName).Returns(armyStrategy);
			return armyStrategy;
		}

		ArmyStrategy GivenAnStrategyForWarriors(ArmyStrategyName army1SettingsArmyStrategyName)
		{
			var armyStrategy = Substitute.For<ArmyStrategy>();
			armyStrategyProvider.ForWarriors(army1SettingsArmyStrategyName).Returns(armyStrategy);
			return armyStrategy;
		}

		List<ArmyUnitPresenter> GivenPresentersWillBeCreatedForUnits(List<ArmyUnit> listOfArchersForArmy1)
		{
			var armyUnitPresenters = new List<ArmyUnitPresenter>();
			listOfArchersForArmy1.ForEach(archer =>
			{
				var armyUnitPresenter = Substitute.For<ArmyUnitPresenter>();
				armyUnitPresenterFactory.Create(archer).Returns(armyUnitPresenter);
				armyUnitPresenters.Add(armyUnitPresenter);
			});

			return armyUnitPresenters;
		}

		List<ArmyUnit> GivenArchersWillBeCreated(int armyId, int armySettingsArcherCount)
		{
			var listOfArmyUnits = new List<ArmyUnit>();
			view.InstantiateArchers(armyId, armySettingsArcherCount).Returns(listOfArmyUnits);
			return listOfArmyUnits;
		}

		List<ArmyUnit> GivenWarriorsWillBeCreated(int armyId, int armySettingsWarriorsCount)
		{
			var listOfArmyUnits = new List<ArmyUnit>();
			view.InstantiateWarriors(armyId, armySettingsWarriorsCount).Returns(listOfArmyUnits);
			return listOfArmyUnits;
		}

		ArmySettings GivenAConfiguredArmy(int armyId)
		{
			var validArmySettings = new ArmySettings()
				{archerCount = armyId + 10, warriorCount = armyId + 20, armyStrategyName = ArmyStrategyName.StrategyBasic};
			armySettingsRepository.Get(armyId).Returns(validArmySettings);

			return validArmySettings;
		}

		void WhenPresents()
		{
			presenter.Present();
		}

		void ThenArmyArchersAreCreated(int armyId, int armySettingsArcherCount)
		{
			view.Received(1).InstantiateArchers(armyId, armySettingsArcherCount);
		}

		void ThenArmyWarriorsAreCreated(int armyId, int armySettingsArcherCount)
		{
			view.Received(1).InstantiateWarriors(armyId, armySettingsArcherCount);
		}

		void ThenUnitsArePresented(List<ArmyUnitPresenter> presentersForArmy1Archers, ArmyStrategy armyStrategy, int army1Id)
		{
			presentersForArmy1Archers.ForEach(t=>t.Received(1).Present(armyStrategy, army1Id));;
		}
	}
}