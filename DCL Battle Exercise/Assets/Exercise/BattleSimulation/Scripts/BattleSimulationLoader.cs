using Exercise.BattleCreation.Infrastructure;
using Exercise.BattleSimulation.Core.Actions;
using Exercise.BattleSimulation.Core.Domain;
using Exercise.BattleSimulation.Core.Domain.Service;
using Exercise.BattleSimulation.Presentation;
using Exercise.BattleSimulation.Presentation.Service;
using Exercise.BattleSimulation.View;
using UnityEngine;

namespace Exercise.BattleSimulation
{
	public class BattleSimulationLoader : MonoBehaviour
	{
		[SerializeField] BattleInstantiator battlefieldPrefab;
		[SerializeField] BattleSettings armySettings;

		void Start()
		{
			var battleService = new BattleService();
			var addUnitsToBattle = new AddUnitsToBattle(battleService);
			var battleCenterRepository = new BattleCenterRepository();
			var getCenterOfBattle = new GetCenterOfBattle(battleCenterRepository);
			var getAllUnits = new GetAllUnits(battleService);
			var basicRulesBehaviour = new BasicRulesBehaviour(getCenterOfBattle, getAllUnits);
			var armyUnitPresenterFactory = new ArmyUnitPresenterFactory(battleService, basicRulesBehaviour);
			BattlefieldView battlefield = Instantiate(battlefieldPrefab);
			var findNearestUnit = new FindNearestEnemy(battleService);
			var armyStrategyFactory = new ArmyStrategyProvider(findNearestUnit);
			var updateBattleCenter = new UpdateBattleCenter(battleService, battleCenterRepository);
			var battlefieldPresenter = new BattlefieldPresenter(battlefield, armySettings, addUnitsToBattle,
				armyUnitPresenterFactory, battleService, armyStrategyFactory, updateBattleCenter);
			battlefieldPresenter.Present();
		}
	}
}