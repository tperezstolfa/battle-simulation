using Exercise.Battle.Scripts;
using Exercise.BattleSimulation.Core.Domain.Service;
using Exercise.BattleSimulation.View;
using JetBrains.Annotations;

namespace Exercise.BattleSimulation.Presentation
{
	public class ArmyUnitPresenterFactory
	{
		readonly BattleService battleService;

		readonly BasicRulesBehaviour basicRulesBehaviour;
		// readonly GetArmy getArmy;
		// readonly GetArmyColor saveArmyColor;

		[UsedImplicitly]
		protected ArmyUnitPresenterFactory()
		{
			//NSubstitute needs a parameterless constructor
		}

		public ArmyUnitPresenterFactory( BattleService battleService, BasicRulesBehaviour basicRulesBehaviour)
		{
			this.battleService = battleService;
			this.basicRulesBehaviour = basicRulesBehaviour;
			// this.getArmy = getArmy;
			// this.saveArmyColor = saveArmyColor;
		}

		public virtual ArmyUnitPresenter Create(ArmyUnit armyUnit)
		{
			return new ArmyUnitPresenter(armyUnit, battleService, basicRulesBehaviour);
		}
	}
}