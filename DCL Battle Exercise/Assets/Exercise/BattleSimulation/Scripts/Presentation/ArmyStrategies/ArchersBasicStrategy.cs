using Exercise.Battle.Scripts;
using Exercise.BattleSimulation.Core.Actions;
using Exercise.BattleSimulation.View;
using UnityEngine;

namespace Exercise.BattleSimulation.Presentation.ArmyStrategies
{
	public class ArchersBasicStrategy : ArmyStrategy
	{
		readonly FindNearestEnemy findNearestEnemy;

		public ArchersBasicStrategy(FindNearestEnemy findNearestEnemy)
		{
			this.findNearestEnemy = findNearestEnemy;
		}

		public void Apply(ArmyUnit unit)
		{
			var nearestEnemy = findNearestEnemy.Invoke(unit);

			if (nearestEnemy == null)
				return;
			
			MoveTowardsNearestEnemy(unit, nearestEnemy);
			AttackNearestEnemy(unit, nearestEnemy);
		}

		static void AttackNearestEnemy(ArmyUnit unit, ArmyUnit nearestEnemy)
		{
			unit.Attack(nearestEnemy);
		}

		static void MoveTowardsNearestEnemy(ArmyUnit unit, ArmyUnit nearestEnemy)
		{
			var unitPosition = unit.GetPosition();
			var enemyPosition = nearestEnemy.GetPosition();

			var distanceToNearestEnemyVector = (enemyPosition - unitPosition);
			var directionToNearestEnemy = new Vector3(distanceToNearestEnemyVector.x, 0, distanceToNearestEnemyVector.z).normalized;
			unit.Move(directionToNearestEnemy);
		}
	}
}