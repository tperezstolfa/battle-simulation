using Exercise.Battle.Scripts;
using Exercise.BattleSimulation.Core.Actions;
using Exercise.BattleSimulation.View;
using UnityEngine;

namespace Exercise.BattleSimulation.Presentation.ArmyStrategies
{
	public class WarriorsBasicStrategy : ArmyStrategy
	{
		readonly FindNearestEnemy findNearestEnemy;

		public WarriorsBasicStrategy(FindNearestEnemy findNearestEnemy)
		{
			this.findNearestEnemy = findNearestEnemy;
		}

		public void Apply(ArmyUnit unit)
		{
			var nearestEnemy = findNearestEnemy.Invoke(unit);

			if (nearestEnemy == null)
				return;

			var enemyPosition = nearestEnemy.GetPosition();
			var unitPosition = unit.GetPosition();

			var distanceToNearestEnemyVector = (enemyPosition - unitPosition);
			var directionToNearestEnemy = new Vector3(distanceToNearestEnemyVector.x, 0, distanceToNearestEnemyVector.z).normalized;
			unit.Move(directionToNearestEnemy);

			unit.Attack(nearestEnemy);
		}
	}
}