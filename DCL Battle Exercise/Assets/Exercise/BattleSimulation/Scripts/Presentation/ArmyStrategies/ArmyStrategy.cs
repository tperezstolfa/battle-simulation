using Exercise.BattleSimulation.View;

namespace Exercise.BattleSimulation.Presentation.ArmyStrategies
{
	public interface ArmyStrategy
	{
		void Apply(ArmyUnit unit);
	}
}