using Exercise.BattleSimulation.Core.Actions;
using Exercise.BattleSimulation.View;
using UnityEngine;

namespace Exercise.BattleSimulation.Presentation.ArmyStrategies
{
	public class ArchersDefensiveStrategy : ArmyStrategy
	{
		readonly FindNearestEnemy findNearestEnemy;

		public ArchersDefensiveStrategy(FindNearestEnemy findNearestEnemy)
		{
			this.findNearestEnemy = findNearestEnemy;
		}

		public void Apply(ArmyUnit unit)
		{
			var nearestEnemy = findNearestEnemy.Invoke(unit);
			
			if (nearestEnemy == null)
				return;
			
			var unitPosition = unit.GetPosition();
			var attackRange = unit.GetRange();
			var enemyPosition = nearestEnemy.GetPosition();

			var distanceToNearestEnemyVector = enemyPosition - unitPosition;
			var distanceToNearestEnemy = Vector3.Magnitude(distanceToNearestEnemyVector);

			if (distanceToNearestEnemy <= attackRange)
			{
				var directionToNearestEnemy = new Vector3(distanceToNearestEnemyVector.x, 0, distanceToNearestEnemyVector.z).normalized;

				var flank = Quaternion.Euler(0, 90, 0) * directionToNearestEnemy;
				unit.Move(-(directionToNearestEnemy + flank).normalized);
				unit.Attack(nearestEnemy);
			}
			else
			{
				var directionToNearestEnemy = new Vector3(distanceToNearestEnemyVector.x, 0, distanceToNearestEnemyVector.z).normalized;
				unit.Move(directionToNearestEnemy);
			}

		}
	}
}