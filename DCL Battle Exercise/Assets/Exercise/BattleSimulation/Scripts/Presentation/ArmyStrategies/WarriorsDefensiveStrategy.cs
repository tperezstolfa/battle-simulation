using Exercise.Battle.Scripts;
using Exercise.BattleSimulation.Core.Actions;
using Exercise.BattleSimulation.View;
using UnityEngine;

namespace Exercise.BattleSimulation.Presentation.ArmyStrategies
{
	public class WarriorsDefensiveStrategy : ArmyStrategy
	{
		readonly FindNearestEnemy findNearestEnemy;

		public WarriorsDefensiveStrategy(FindNearestEnemy findNearestEnemy)
		{
			this.findNearestEnemy = findNearestEnemy;
		}

		public void Apply(ArmyUnit unit)
		{
			var nearestEnemy = findNearestEnemy.Invoke(unit);

			if (nearestEnemy == null)
				return;

			var attackCooldown = unit.GetCooldown();
			var enemyPosition = nearestEnemy.GetPosition();
			var unitPosition = unit.GetPosition();
			var distanceToNearestEnemyVector = (enemyPosition - unitPosition);
			var directionToNearestEnemy = new Vector3(distanceToNearestEnemyVector.x, 0, distanceToNearestEnemyVector.z).normalized;

			if (attackCooldown <= 0)
			{
				unit.Move(directionToNearestEnemy);
				unit.Attack(nearestEnemy);
			}
			else
			{
				unit.Move(directionToNearestEnemy * -1);
			}
		}
	}
}