using System.Collections.Generic;
using Exercise.BattleCreation.Core.Domain;
using Exercise.BattleSimulation.Core.Actions;
using Exercise.BattleSimulation.Core.Domain.Service;
using Exercise.BattleSimulation.Presentation.ArmyStrategies;
using Exercise.BattleSimulation.Presentation.Service;
using Exercise.BattleSimulation.View;

namespace Exercise.BattleSimulation.Presentation
{
	public class BattlefieldPresenter
	{
		readonly BattlefieldView view;
		readonly ArmySettingsRepository armySettingsRepository;
		readonly AddUnitsToBattle addUnitsToBattle;
		readonly ArmyUnitPresenterFactory armyUnitPresenterFactory;
		readonly BattleService battleService;
		readonly ArmyStrategyProvider armyStrategyProvider;
		readonly UpdateBattleCenter updateBattleCenter;

		public BattlefieldPresenter(BattlefieldView view,
			ArmySettingsRepository armySettingsRepository,
			AddUnitsToBattle addUnitsToBattle,
			ArmyUnitPresenterFactory armyUnitPresenterFactory,
			BattleService battleService,
			ArmyStrategyProvider armyStrategyProvider, 
			UpdateBattleCenter updateBattleCenter)
		{
			this.view = view;
			this.armySettingsRepository = armySettingsRepository;
			this.addUnitsToBattle = addUnitsToBattle;
			this.armyUnitPresenterFactory = armyUnitPresenterFactory;
			this.battleService = battleService;
			this.armyStrategyProvider = armyStrategyProvider;
			this.updateBattleCenter = updateBattleCenter;

			view.OnUpdate += OnBattleUpdate;
		}

		public void Present()
		{
			InstantiateArmy(0);
			InstantiateArmy(1);
			view.SetArmies(battleService.GetArmy(0), battleService.GetArmy(1));
		}

		void OnBattleUpdate()
		{
			var battleCenter = updateBattleCenter.Invoke();
			view.MoveCamera(battleCenter);
		}

		void InstantiateArmy(int armyId)
		{
			var armySettings = armySettingsRepository.Get(armyId);
			InstantiateAndPresentArchers(armyId, armySettings);
			InstantiateAndPresentWarriors(armyId, armySettings);
		}

		void InstantiateAndPresentWarriors(int armyId, ArmySettings armySettings)
		{
			var warriors = view.InstantiateWarriors(armyId, armySettings.warriorCount);
			var warriorsStrategy = armyStrategyProvider.ForWarriors(armySettings.armyStrategyName);
			AddUnitsAndPresent(armyId, warriors, warriorsStrategy);
		}

		void InstantiateAndPresentArchers(int armyId, ArmySettings armySettings)
		{
			var archers = view.InstantiateArchers(armyId, armySettings.archerCount);
			var archersStrategy = armyStrategyProvider.ForArchers(armySettings.armyStrategyName);
			AddUnitsAndPresent(armyId, archers, archersStrategy);
		}

		void AddUnitsAndPresent(int armyId, List<ArmyUnit> units, ArmyStrategy armyStrategy)
		{
			addUnitsToBattle.Invoke(armyId, units);
			PresentUnits(armyId, units, armyStrategy);
		}

		void PresentUnits(int armyId, List<ArmyUnit> units, ArmyStrategy armyStrategy)
		{
			units.ForEach(armyUnit =>
			{
				var armyUnitPresenter = armyUnitPresenterFactory.Create(armyUnit);
				armyUnitPresenter.Present(armyStrategy, armyId);
			});
		}
	}
}