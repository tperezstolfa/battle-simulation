using Exercise.BattleCreation.Core.Domain;
using Exercise.BattleSimulation.Core.Actions;
using Exercise.BattleSimulation.Core.Domain.Service;
using Exercise.BattleSimulation.Presentation.ArmyStrategies;
using JetBrains.Annotations;

namespace Exercise.BattleSimulation.Presentation.Service
{
	public class ArmyStrategyProvider
	{
		readonly ArmyStrategy archersDefensive;
		readonly ArmyStrategy archersBasic;
		readonly ArmyStrategy warriorsDefensive;
		readonly ArmyStrategy warriorsBasic;

		[UsedImplicitly]
		protected ArmyStrategyProvider()
		{
			//NSubstitute needs a parameterless constructor
		}

		public ArmyStrategyProvider(FindNearestEnemy findNearestEnemy)
		{
			archersDefensive = new ArchersDefensiveStrategy(findNearestEnemy);
			archersBasic = new ArchersBasicStrategy(findNearestEnemy);
			warriorsDefensive = new WarriorsDefensiveStrategy(findNearestEnemy);
			warriorsBasic = new WarriorsBasicStrategy(findNearestEnemy);
		}

		public virtual ArmyStrategy ForArchers(ArmyStrategyName strategyName)
		{
			switch (strategyName)
			{
				case ArmyStrategyName.StrategyDefensive:
					return archersDefensive;
				default:
					return archersBasic;
			}
		}

		public virtual ArmyStrategy ForWarriors(ArmyStrategyName strategyName)
		{
			switch (strategyName)
			{
				case ArmyStrategyName.StrategyDefensive:
					return warriorsDefensive;
				default:
					return warriorsBasic;
			}
		}
	}
}