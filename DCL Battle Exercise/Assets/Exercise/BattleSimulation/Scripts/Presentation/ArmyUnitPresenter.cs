using Cysharp.Threading.Tasks;
using Exercise.BattleSimulation.Core.Domain.Service;
using Exercise.BattleSimulation.Presentation.ArmyStrategies;
using Exercise.BattleSimulation.View;

namespace Exercise.BattleSimulation.Presentation
{
	public class ArmyUnitPresenter
	{
		readonly ArmyUnit armyUnit;
		readonly BattleService battleService;
		readonly BasicRulesBehaviour basicRulesBehaviour;

		public ArmyUnitPresenter(
			ArmyUnit armyUnit, 
			BattleService battleService,
			BasicRulesBehaviour basicRulesBehaviour)
		{
			this.armyUnit = armyUnit;
			this.battleService = battleService;
			this.basicRulesBehaviour = basicRulesBehaviour;
		}

		public void Present(ArmyStrategy armyStrategy, int armyId)
		{
			armyUnit.OnUpdate += () => FollowStrategyAndBasicRules(armyStrategy);
			var army = battleService.GetArmy(armyId);
			armyUnit.SetArmy(army);
			armyUnit.SetArmyId(armyId);
		}

		void FollowStrategyAndBasicRules(ArmyStrategy armyStrategy)
		{
			armyStrategy.Apply(armyUnit);
			basicRulesBehaviour.Apply(armyUnit);
		}
	}
}