using Exercise.BattleSimulation.Core.Actions;
using Exercise.BattleSimulation.View;
using JetBrains.Annotations;
using UnityEngine;

namespace Exercise.BattleSimulation.Presentation
{
	public class BasicRulesBehaviour
	{
		readonly GetCenterOfBattle getCenterOfBattle;
		readonly GetAllUnits getAllUnits;

		[UsedImplicitly]
		protected BasicRulesBehaviour()
		{
			//NSubstitute needs a parameterless constructor
		}

		public BasicRulesBehaviour(GetCenterOfBattle getCenterOfBattle, GetAllUnits getAllUnits)
		{
			this.getCenterOfBattle = getCenterOfBattle;
			this.getAllUnits = getAllUnits;
		}

		public virtual void Apply(ArmyUnit unitBase)
		{
			var allUnits = getAllUnits.Invoke();

			var thisTransform = unitBase.GetTransform();
			var position = thisTransform.position;

			var centerOfBattle = getCenterOfBattle.Invoke();
			var distanceToCenter = Vector3.Distance(position, centerOfBattle);

			if (distanceToCenter > 80.0f)
			{
				var directionToCenter = (centerOfBattle - position).normalized;
				thisTransform.position -= directionToCenter * (80.0f - distanceToCenter);
				return;
			}

			//TODO Should use a KDTree to get only the closest units the quickest possible
			foreach (var armyUnit in allUnits)
			{
				var otherUnitPosition = armyUnit.GetPosition();
				var distance = Vector3.Distance(position, otherUnitPosition);

				if (distance < 2f)
				{
					var directionToNearest = (otherUnitPosition - position).normalized;
					position -= directionToNearest * (2.0f - distance);
					thisTransform.position = position;
				}
			}
		}
	}
}