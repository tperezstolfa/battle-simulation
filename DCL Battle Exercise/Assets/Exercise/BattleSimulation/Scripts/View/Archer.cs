﻿using UnityEngine;

namespace Exercise.BattleSimulation.View
{
    public class Archer : UnitBase
    {
        public ArcherArrow arrowPrefab;

        void Awake()
        {
            health = 5;
            defense = 0;
            attack = 10;
            attackRange = 20f;
            maxAttackCooldown = 5f;
            postAttackDelay = 1f;
        }

        public override void Attack(ArmyUnit enemy)
        {
            if ( attackCooldown > 0 )
                return;

            if ( Vector3.Distance(transform.position, enemy.GetTransform().position) > attackRange )
                return;

            attackCooldown = maxAttackCooldown;
            GameObject arrow = Instantiate(arrowPrefab.gameObject);
            arrow.GetComponent<ArcherArrow>().target = enemy.GetTransform().position;
            arrow.GetComponent<ArcherArrow>().attack = attack;
            arrow.GetComponent<ArcherArrow>().army = army;
            arrow.transform.position = transform.position;

            var animator = GetComponentInChildren<Animator>();
            animator?.SetTrigger("Attack");
            
            arrow.GetComponent<Renderer>().material.color = BattleInstantiator.instance.GetArmyColor(army.Id);
        }

        public void OnDeathAnimFinished()
        {
            Destroy(gameObject);
        }
    }
}