﻿using UnityEngine;

namespace Exercise.BattleSimulation.View
{
	public class Warrior : UnitBase
	{
		void Awake()
		{
			health = 50;
			defense = 5;
			attack = 20;
			attackRange = 2.5f;
			maxAttackCooldown = 1f;
			postAttackDelay = 0;
		}

		public override void Attack(ArmyUnit enemy)
		{
			if (attackCooldown > 0)
				return;

			if (Vector3.Distance(transform.position, enemy.GetTransform().position) > attackRange)
				return;

			var targetUnit = enemy as UnitBase;

			if (targetUnit == null)
				return;

			attackCooldown = maxAttackCooldown;

			var animator = GetComponentInChildren<Animator>();
			animator.SetTrigger("Attack");

			targetUnit.ReceiveDamage(attack, transform.position);
		}

		public void OnDeathAnimFinished()
		{
			Destroy(gameObject);
		}
	}
}