﻿using System;
using Exercise.BattleSimulation.Core.Domain;
using Exercise.BattleSimulation.Presentation;
using Exercise.BattleSimulation.Presentation.ArmyStrategies;
using UnityEngine;

namespace Exercise.BattleSimulation.View
{
    public abstract class UnitBase : MonoBehaviour, ArmyUnit
    {
        public float health { get; protected set; }
        public float defense { get; protected set; }
        public float attack { get; protected set; }
        public float attackRange { get; protected set; }
        public float maxAttackCooldown { get; protected set; }
        public float postAttackDelay { get; protected set; }
        public float speed { get; protected set; } = 0.1f;
        int armyId;
        public event Action OnUpdate = () => {};

        [SerializeField] Animator animator;

        public Army army;

        protected float attackCooldown;
        Vector3 lastPosition;

        public abstract void Attack(ArmyUnit enemy);
        public float GetCooldown()
        {
            return attackCooldown;
        }

        public virtual void Move( Vector3 direction )
        {
            if (attackCooldown > maxAttackCooldown - postAttackDelay)
                return;

            transform.position += direction * speed;
        }

        public virtual void ReceiveDamage(float sourceAttack, Vector3 sourcePosition)
        {
            health -= Mathf.Max(sourceAttack - defense, 0);

            if ( health < 0 )
            {
                var thisTransform = transform;
                thisTransform.forward = sourcePosition - thisTransform.position;

                army.RemoveUnit(this);

                animator?.SetTrigger("Death");
            }
            else
            {
                animator?.SetTrigger("Hit");
            }
        }

        void Update()
        {
            if ( health < 0 )
                return;
            
            attackCooldown -= Time.deltaTime;

            OnUpdate();

            var position = transform.position;
            animator.SetFloat("MovementSpeed", (position - lastPosition).magnitude / speed);
            lastPosition = position;
        }
        
        public void SetArmy(Army army)
        {
            this.army = army;
        }

        public void SetArmyId(int armyId)
        {
            this.armyId = armyId;
        }

        public int GetArmyId()
        {
            return armyId;
        }

        public Transform GetTransform()
        {
            return transform;
        }

        public float GetRange()
        {
            return attackRange;
        }

        public Vector3 GetPosition()
        {
            return transform.position;
        }
    }

    public interface ArmyUnit
    {
        event Action OnUpdate;
        void SetArmyId(int armyId);
        Vector3 GetPosition();
        float GetRange();
        void Move(Vector3 direction);
        void Attack(ArmyUnit enemy);
        float GetCooldown();
        int GetArmyId();
        
        //TODO These two methods SetArmy and GetTransform are temporary until I finish the refactor
        void SetArmy(Army army);
        Transform GetTransform();
    }
}