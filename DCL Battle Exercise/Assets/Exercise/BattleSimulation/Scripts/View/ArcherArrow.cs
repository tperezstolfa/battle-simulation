using System;
using Exercise.BattleSimulation.Core.Domain;
using UnityEngine;

namespace Exercise.BattleSimulation.View
{
    public class ArcherArrow : MonoBehaviour
    {
        public float speed;

        [NonSerialized] public Vector3 target;
        [NonSerialized] public float attack;

        public Army army;

        public void Update()
        {
            var arrowTransform = transform;
            var arrowPosition = arrowTransform.position;
            var direction = (target - arrowPosition).normalized;
            arrowPosition += direction * speed;
            arrowTransform.position = arrowPosition;
            arrowTransform.forward = direction;

            foreach ( var enemyUnit in army.enemyArmy.GetUnits() )
            {
                float dist = Vector3.Distance(enemyUnit.transform.position, arrowPosition);

                if (dist < speed)
                {
                    enemyUnit.ReceiveDamage(attack,transform.position);
                    Destroy(gameObject);
                    return;
                }
            }

            if ( Vector3.Distance(arrowPosition, target) < speed)
            {
                Destroy(gameObject);
            }
        }
    }
}