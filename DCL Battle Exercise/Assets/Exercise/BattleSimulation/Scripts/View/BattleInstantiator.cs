using System;
using System.Collections.Generic;
using System.Linq;
using Exercise.Battle.Scripts;
using Exercise.BattleCreation.Core.Domain;
using Exercise.BattleCreation.Infrastructure;
using Exercise.BattleSimulation.Core.Domain;
using UnityEngine;

namespace Exercise.BattleSimulation.View
{
    public class BattleInstantiator : MonoBehaviour, BattlefieldView
    {
        public static BattleInstantiator instance { get; private set; }
        [SerializeField] Warrior warriorPrefab;
        [SerializeField] Archer archerPrefab;
        [SerializeField] BoxCollider leftArmySpawnBounds;
        [SerializeField] BoxCollider rightArmySpawnBounds;

        public Army army1 ;
        public Army army2;

        public Color army1Color;
        public Color army2Color;

        public GameOverMenu gameOverMenu;
        
        Vector3 forwardTarget;
        public event Action OnUpdate = () => {};
        void Awake()
        {
            instance = this;
        }

        Bounds GetBoundsForArmy(int armyId)
        {
            return armyId == 1 ? leftArmySpawnBounds.bounds : rightArmySpawnBounds.bounds;
        }

        void Update()
        {
            if ( army1.GetUnits().Count == 0 || army2.GetUnits().Count == 0 )
            {
                gameOverMenu.gameObject.SetActive(true);
                gameOverMenu.Populate();
            }

            OnUpdate();
        }



        public List<ArmyUnit> InstantiateArchers(int armyId, int archerCount)
        {
            var listOfUnits = new List<ArmyUnit>();
            for ( var i = 0; i < archerCount; i++ )
            {
                var go = Instantiate(archerPrefab.gameObject);
                go.transform.position = Utils.GetRandomPosInBounds(GetBoundsForArmy(armyId));

                go.GetComponentInChildren<Renderer>().material.color = army1Color;

                listOfUnits.Add(go.GetComponent<Archer>());
                go.GetComponentInChildren<Renderer>().material.color = GetArmyColor(armyId);
            }

            return listOfUnits;
        }

        public Color GetArmyColor(int armyId)
        {
           return armyId == 0 ? army1Color : army2Color;
        }

        public List<ArmyUnit> InstantiateWarriors(int armyId, int warriorsCount)
        {
            var listOfUnits = new List<ArmyUnit>();
            for ( var i = 0; i < warriorsCount; i++ )
            {
                var go = Instantiate(warriorPrefab.gameObject);
                go.transform.position = Utils.GetRandomPosInBounds(GetBoundsForArmy(armyId));

                go.GetComponentInChildren<Renderer>().material.color = army1Color;

                listOfUnits.Add(go.GetComponent<Warrior>());
                go.GetComponentInChildren<Renderer>().material.color = GetArmyColor(armyId);
            }

            return listOfUnits;
        }

        public void SetArmies(Army a1, Army a2)
        {
            army1 = a1;
            army2 = a2;
        }

        public void MoveCamera(Vector3 battleCenter)
        {
            var cameraTransform = Camera.main.transform;
            forwardTarget = (battleCenter - cameraTransform.position).normalized;

            var cameraForward = cameraTransform.forward;
            cameraForward += (forwardTarget - cameraForward) * 0.1f;
            cameraTransform.forward = cameraForward;
        }
    }

    public interface BattlefieldView
    {
        event Action OnUpdate;
        List<ArmyUnit> InstantiateArchers(int armyId, int archerCount);
        List<ArmyUnit> InstantiateWarriors(int armyId, int warriorsCount);
        //TODO Temporary method to try new implementation until refactor is completed
        void SetArmies(Army a1, Army a2);
        void MoveCamera(Vector3 battleCenter);
    }
}