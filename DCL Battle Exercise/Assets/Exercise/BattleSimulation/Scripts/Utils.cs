using System.Collections.Generic;
using Exercise.BattleSimulation.View;
using UnityEngine;

namespace Exercise.Battle.Scripts
{
    public static class Utils
    {
        public static Vector3 GetRandomPosInBounds(Bounds bounds)
        {
            Vector3 pos = Vector3.zero;
            pos.x = Random.Range( bounds.min.x, bounds.max.x );
            pos.z = Random.Range( bounds.min.z, bounds.max.z );
            return pos;
        }

        public static float GetNearestObject( ArmyUnit source, List<UnitBase> objects, out UnitBase nearestObject )
        {
            float minDist = float.MaxValue;
            nearestObject = null;

            foreach ( var obj in objects )
            {
                float dist = Vector3.Distance(source.GetPosition(), obj.GetPosition());

                if ( dist < minDist )
                {
                    minDist = dist;
                    nearestObject = obj;
                }
            }

            return minDist;
        }
    }
}