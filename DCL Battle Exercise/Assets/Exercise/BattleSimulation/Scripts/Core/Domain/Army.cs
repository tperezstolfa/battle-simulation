﻿using System.Collections.Generic;
using System.Linq;
using Exercise.Battle.Scripts;
using Exercise.BattleSimulation.View;

namespace Exercise.BattleSimulation.Core.Domain
{
    public class Army
    {
        public Army enemyArmy;
        public ArmyUnit[] Units;
        public int Id;

        public List<UnitBase> GetUnits()
        {
            return Units.Select(unit => unit as UnitBase).ToList();
        }

        public void RemoveUnit(UnitBase unitBase)
        {
            var armyUnits = Units.ToList();
            armyUnits.Remove(unitBase);
            Units = armyUnits.ToArray();
        }
    }
}