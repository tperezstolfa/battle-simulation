namespace Exercise.BattleSimulation.Core.Domain
{
	public class Battle
	{
		public readonly Army[] Armies;

		public Battle()
		{
			//TODO temporary fix to attach current refactor in progress to old untested code
			var army1 = new Army();
			var army2 = new Army();
			army1.enemyArmy = army2;
			army2.enemyArmy = army1;
			Armies = new[]{army1, army2};
		}
	}
}