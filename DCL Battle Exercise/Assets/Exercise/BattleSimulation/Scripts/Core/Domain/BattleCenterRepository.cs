using UnityEngine;

namespace Exercise.BattleSimulation.Core.Domain
{
	public class BattleCenterRepository
	{
		Vector3 battleCenter;

		public void Put(Vector3 battleCenter)
		{
			this.battleCenter = battleCenter;
		}

		public Vector3 Get()
		{
			return battleCenter;
		}
	}
}