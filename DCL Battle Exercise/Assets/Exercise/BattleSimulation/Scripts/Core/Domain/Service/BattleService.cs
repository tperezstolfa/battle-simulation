using System.Collections.Generic;
using System.Linq;
using Exercise.BattleSimulation.View;

namespace Exercise.BattleSimulation.Core.Domain.Service
{
	public class BattleService
	{
		readonly Battle battle = new Battle();

		public virtual void RegisterUnits(int armyId, List<ArmyUnit> units)
		{
			var battleArmy = battle.Armies[armyId];
			battleArmy.Id = armyId;
			var armyUnits = battleArmy.Units;

			battleArmy.Units = armyUnits == null ? units.ToArray() : armyUnits.Union(units).ToArray();
		}

		public List<ArmyUnit> GetUnits(int armyId)
		{
			return battle.Armies[armyId].Units.ToList();
		}

		//TODO This method may be temporary, so I can test the already refactored classes before I continue
		public Army GetArmy(int armyId)
		{
			return battle.Armies[armyId];
		}

		public virtual List<ArmyUnit> GetEnemies(int armyId)
		{
			var enemies = new List<ArmyUnit>();
			foreach (var army in battle.Armies)
			{
				if (army.Id != armyId)
				{
					enemies.AddRange(army.Units);
				}
			}

			return enemies;
		}

		public virtual List<ArmyUnit> GetAllUnits()
		{
			var units = new List<ArmyUnit>();
			foreach (var army in battle.Armies)
			{
				units.AddRange(army.Units);
			}

			return units;
		}
	}
}