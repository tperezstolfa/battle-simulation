using System.Linq;
using Exercise.Battle.Scripts;
using Exercise.BattleSimulation.Core.Domain;
using Exercise.BattleSimulation.Core.Domain.Service;
using Exercise.BattleSimulation.View;
using JetBrains.Annotations;
using UnityEngine;

namespace Exercise.BattleSimulation.Core.Actions
{
	public class GetCenterOfBattle
	{
		readonly BattleCenterRepository battleCenterRepository;

		[UsedImplicitly]
		protected GetCenterOfBattle()
		{
			//NSubstitute needs a parameterless constructor
		}
		
		public GetCenterOfBattle(BattleCenterRepository battleCenterRepository)
		{
			this.battleCenterRepository = battleCenterRepository;
		}

		public Vector3 Invoke()
		{
			return battleCenterRepository.Get();
		}
	}
}