using System.Linq;
using Exercise.Battle.Scripts;
using Exercise.BattleSimulation.Core.Domain.Service;
using Exercise.BattleSimulation.View;
using JetBrains.Annotations;

namespace Exercise.BattleSimulation.Core.Actions
{
	public class FindNearestEnemy
	{
		readonly BattleService battleService;

		[UsedImplicitly]
		protected FindNearestEnemy()
		{
			//NSubstitute needs a parameterless constructor
		}

		public FindNearestEnemy(BattleService battleService)
		{
			this.battleService = battleService;
		}

		[CanBeNull]
		public virtual ArmyUnit Invoke(ArmyUnit unit)
		{
			//TODO Should use a KDTree to get the closest enemy the quickest possible
			var enemies = battleService.GetEnemies(unit.GetArmyId());
			var unitBases = enemies.Select(unit => unit as UnitBase).ToList();
			Utils.GetNearestObject(unit, unitBases, out UnitBase nearestEnemy);
			return nearestEnemy;
		}
	}
}