using System.Collections.Generic;
using System.Linq;
using Exercise.Battle.Scripts;
using Exercise.BattleSimulation.Core.Domain;
using Exercise.BattleSimulation.Core.Domain.Service;
using Exercise.BattleSimulation.View;
using JetBrains.Annotations;
using UnityEngine;

namespace Exercise.BattleSimulation.Core.Actions
{
	public class UpdateBattleCenter
	{
		readonly BattleService battleService;
		readonly BattleCenterRepository battleCenterRepository;

		[UsedImplicitly]
		protected UpdateBattleCenter()
		{
			//NSubstitute needs a parameterless constructor
		}

		public UpdateBattleCenter(BattleService battleService, BattleCenterRepository battleCenterRepository)
		{
			this.battleService = battleService;
			this.battleCenterRepository = battleCenterRepository;
		}

		public virtual Vector3 Invoke()
		{
			var units = battleService.GetAllUnits();

			var battleCenter = CalculateCurrentBattleCenter(units);
			battleCenterRepository.Put(battleCenter);
			
			return battleCenter;
		}

		static Vector3 CalculateCurrentBattleCenter(List<ArmyUnit> units)
		{
			var resultX = 0f;
			var resultY = 0f;
			var resultZ = 0f;

			foreach (var armyUnit in units)
			{
				var position = armyUnit.GetPosition();
				resultX += position.x;
				resultY += position.y;
				resultZ += position.z;
			}

			var battleCenter = new Vector3(resultX / units.Count, resultY / units.Count, resultZ / units.Count);
			return battleCenter;
		}
	}
}