using System.Collections.Generic;
using Exercise.Battle.Scripts;
using Exercise.BattleSimulation.Core.Domain.Service;
using Exercise.BattleSimulation.View;
using JetBrains.Annotations;

namespace Exercise.BattleSimulation.Core.Actions
{
	public class AddUnitsToBattle
	{
		readonly BattleService battleService;

		[UsedImplicitly]
		protected AddUnitsToBattle()
		{
			//NSubstitute needs a parameterless constructor
		}

		public AddUnitsToBattle(BattleService battleService)
		{
			this.battleService = battleService;
		}

		public virtual void Invoke(int armyId, List<ArmyUnit> listOfUnits)
		{
			battleService.RegisterUnits(armyId, listOfUnits);
		}
	}
}