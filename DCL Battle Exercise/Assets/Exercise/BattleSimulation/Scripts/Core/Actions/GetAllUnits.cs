using System.Collections.Generic;
using Exercise.BattleSimulation.Core.Domain.Service;
using Exercise.BattleSimulation.View;
using JetBrains.Annotations;

namespace Exercise.BattleSimulation.Core.Actions
{
	public class GetAllUnits
	{
		readonly BattleService battleService;

		[UsedImplicitly]
		protected GetAllUnits()
		{
			//NSubstitute needs a parameterless constructor
		}
		
		public GetAllUnits(BattleService battleService)
		{
			this.battleService = battleService;
		}

		public List<ArmyUnit> Invoke()
		{
			return battleService.GetAllUnits();
		}
	}
}