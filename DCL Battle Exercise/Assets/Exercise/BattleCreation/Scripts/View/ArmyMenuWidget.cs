using Cysharp.Threading.Tasks;
using Cysharp.Threading.Tasks.Linq;
using Exercise.BattleCreation.Core.Domain;
using Exercise.BattleCreation.Infrastructure;
using UnityEngine;
using UnityEngine.UI;

namespace Exercise.BattleCreation.View
{
	public class ArmyMenuWidget : MonoBehaviour, ArmyMenuView
	{
		public BattleSettings settings;
		public int armyId;
		public Slider warriorsCount;
		public Slider archersCount;
		public TMPro.TextMeshProUGUI warriorsCountLabel;
		public TMPro.TextMeshProUGUI archersCountLabel;
		public Toggle defensiveStrategyToggle;
		public Toggle basicStrategyToggle;


		void Update()
		{
			warriorsCountLabel.text = warriorsCount.value.ToString();
			archersCountLabel.text = archersCount.value.ToString();
			warriorsCount.maxValue = 100;
			archersCount.maxValue = 100;
		}

		public void Show(ArmySettings armySettings)
		{
			warriorsCountLabel.text = warriorsCount.value.ToString();
			archersCountLabel.text = archersCount.value.ToString();
			warriorsCount.value = armySettings.warriorCount;
			archersCount.value = armySettings.archerCount;
			defensiveStrategyToggle.isOn = armySettings.armyStrategyName == ArmyStrategyName.StrategyDefensive;
			basicStrategyToggle.isOn = armySettings.armyStrategyName == ArmyStrategyName.StrategyBasic;
		}

		public virtual IUniTaskAsyncEnumerable<int> OnArchersCountChanged()
		{
			return archersCount.OnValueChangedAsAsyncEnumerable().Select(floatValue => (int) floatValue);
		}

		public virtual IUniTaskAsyncEnumerable<int> OnWarriorsCountChanged()
		{
			return warriorsCount.OnValueChangedAsAsyncEnumerable().Select(floatValue => (int) floatValue);
		}

		public virtual IUniTaskAsyncEnumerable<ArmyStrategyName> OnArmyStrategyChanged()
		{
			return defensiveStrategyToggle.OnValueChangedAsAsyncEnumerable()
				.Select(isActive => isActive ? ArmyStrategyName.StrategyDefensive : ArmyStrategyName.StrategyBasic);
		}
	}

	public interface ArmyMenuView
	{
		void Show(ArmySettings armySettings);
		IUniTaskAsyncEnumerable<int> OnArchersCountChanged();
		IUniTaskAsyncEnumerable<int> OnWarriorsCountChanged();
		IUniTaskAsyncEnumerable<ArmyStrategyName> OnArmyStrategyChanged();
	}
}