using Cysharp.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

namespace Exercise.BattleCreation.View
{
    public class LaunchMenu : MonoBehaviour, LaunchMenuView
    {
        [SerializeField] Button startButton;
        [SerializeField] ArmyMenuWidget army1Menu;
        [SerializeField] ArmyMenuWidget army2Menu;

        public async UniTask OnRequestBattleStart()
        { 
            await startButton.OnClickAsync();
        }

        public ArmyMenuView GetArmy1Menu()
        {
            return army1Menu;
        }

        public ArmyMenuView GetArmy2Menu()
        {
            return army2Menu;
        }
    }

    public interface LaunchMenuView
    {
        UniTask OnRequestBattleStart();
        ArmyMenuView GetArmy1Menu();
        ArmyMenuView GetArmy2Menu();
    }
}