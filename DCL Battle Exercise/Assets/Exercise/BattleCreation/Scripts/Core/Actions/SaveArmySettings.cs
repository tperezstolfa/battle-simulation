using Exercise.BattleCreation.Core.Domain;
using JetBrains.Annotations;

namespace Exercise.BattleCreation.Core.Actions
{
	public class SaveArmySettings
	{
		readonly ArmySettingsRepository armySettingsRepository;

		[UsedImplicitly]
		protected SaveArmySettings()
		{
			//NSubstitute needs a parameterless constructor
		}

		public SaveArmySettings(ArmySettingsRepository armySettingsRepository)
		{
			this.armySettingsRepository = armySettingsRepository;
		}

		public virtual void Invoke(int armyId, ArmySettings armySettings)
		{
			armySettingsRepository.Save(armyId, armySettings);
		}
	}
}