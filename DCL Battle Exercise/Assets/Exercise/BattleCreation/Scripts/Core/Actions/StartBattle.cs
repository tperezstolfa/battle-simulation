using UnityEngine.SceneManagement;

namespace Exercise.BattleCreation.Core.Actions
{
	public class StartBattle
	{
		public virtual void Invoke()
		{
			SceneManager.LoadScene(1);
		}
	}
}