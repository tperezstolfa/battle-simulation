using Exercise.BattleCreation.Core.Domain;
using JetBrains.Annotations;

namespace Exercise.BattleCreation.Core.Actions
{
	public class GetArmySettings
	{
		readonly ArmySettingsRepository armySettingsRepository;

		[UsedImplicitly]
		protected GetArmySettings()
		{
			//NSubstitute needs a parameterless constructor
		}

		public GetArmySettings(ArmySettingsRepository armySettingsRepository)
		{
			this.armySettingsRepository = armySettingsRepository;
		}

		public virtual ArmySettings Invoke(int armyId)
		{
			return armySettingsRepository.Get(armyId);
		}
	}
}