namespace Exercise.BattleCreation.Core.Domain
{
	[System.Serializable]
	public class ArmySettings
	{
		public int warriorCount;
		public int archerCount;
		public ArmyStrategyName armyStrategyName;

		protected bool Equals(ArmySettings other)
		{
			return warriorCount == other.warriorCount && archerCount == other.archerCount &&
			       armyStrategyName == other.armyStrategyName;
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			if (obj.GetType() != this.GetType()) return false;
			return Equals((ArmySettings) obj);
		}

		public override int GetHashCode()
		{
			unchecked
			{
				var hashCode = warriorCount;
				hashCode = (hashCode * 397) ^ archerCount;
				hashCode = (hashCode * 397) ^ (int) armyStrategyName;
				return hashCode;
			}
		}

		public ArmySettings ChangeArchersCount(int archersCount)
		{
			return new ArmySettings()
			{
				archerCount = archersCount,
				warriorCount = warriorCount,
				armyStrategyName = armyStrategyName
			};
		}

		public ArmySettings ChangeWarriorsCount(int warriorsCount)
		{
			return new ArmySettings()
			{
				archerCount = archerCount,
				warriorCount = warriorsCount,
				armyStrategyName = armyStrategyName
			};
		}

		public ArmySettings ChangeArmyStrategy(ArmyStrategyName armyStrategyName)
		{
			return new ArmySettings()
			{
				archerCount = archerCount,
				warriorCount = warriorCount,
				armyStrategyName = armyStrategyName
			};
		}
	}

	public enum ArmyStrategyName
	{
		None,
		StrategyDefensive,
		StrategyBasic
	}
}