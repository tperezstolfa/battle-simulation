namespace Exercise.BattleCreation.Core.Domain
{
	public interface ArmySettingsRepository
	{
		ArmySettings Get(int armyId);
		void Save(int armyId, ArmySettings armySettings);
	}
}