using Exercise.BattleCreation.Core.Domain;
using UnityEngine;

namespace Exercise.BattleCreation.Infrastructure
{
	[CreateAssetMenu(menuName = "Create BattleSettings", fileName = "BattleSettings", order = 0)]
	public class BattleSettings : ScriptableObject, ArmySettingsRepository
	{
		public ArmySettings army1Settings;
		public ArmySettings army2Settings;

		public ArmySettings Get(int armyId)
		{
			return armyId == 1 ? army1Settings : army2Settings;
		}

		public void Save(int armyId, ArmySettings armySettings)
		{
			if (armyId == 1)
			{
				army1Settings = armySettings;
			}
			else
			{
				army2Settings = armySettings;
			}
		}
	}
}