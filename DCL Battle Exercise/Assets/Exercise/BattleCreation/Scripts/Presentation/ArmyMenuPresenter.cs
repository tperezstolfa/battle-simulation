using Cysharp.Threading.Tasks.Linq;
using Exercise.BattleCreation.Core.Actions;
using Exercise.BattleCreation.Core.Domain;
using Exercise.BattleCreation.View;
using JetBrains.Annotations;

namespace Exercise.BattleCreation.Presentation
{
	public class ArmyMenuPresenter
	{
		readonly ArmyMenuView view;
		readonly GetArmySettings getArmySettings;
		readonly SaveArmySettings saveArmySettings;

		[UsedImplicitly]
		protected ArmyMenuPresenter()
		{
			//NSubstitute needs a parameterless constructor
		}

		public ArmyMenuPresenter(ArmyMenuView view, GetArmySettings getArmySettings, SaveArmySettings saveArmySettings)
		{
			this.view = view;
			this.getArmySettings = getArmySettings;
			this.saveArmySettings = saveArmySettings;
		}

		public virtual void Present(int armyId)
		{
			var armySettings = getArmySettings.Invoke(armyId);
			ShowArmySettings(armySettings);

			ListenToArcherSettingsModification(armyId);
			ListenToWarriorSettingsModification(armyId);
			ListenToArmyStrategySettingsModification(armyId);
		}

		void ShowArmySettings(ArmySettings armySettings)
		{
			view.Show(armySettings);
		}

		void ListenToArcherSettingsModification(int armyId)
		{
			view.OnArchersCountChanged().ForEachAsync(archersCount =>
			{
				var armySettings = getArmySettings.Invoke(armyId);
				var newArmySettings = armySettings.ChangeArchersCount(archersCount);
				SaveAndShowArmySettings(armyId, newArmySettings);
			});
		}
		
		void ListenToWarriorSettingsModification(int armyId)
		{
			view.OnWarriorsCountChanged().ForEachAsync(warriorsCount =>
			{
				var armySettings = getArmySettings.Invoke(armyId);
				var newArmySettings = armySettings.ChangeWarriorsCount(warriorsCount);
				SaveAndShowArmySettings(armyId, newArmySettings);
			});
		}
		
		void ListenToArmyStrategySettingsModification(int armyId)
		{
			view.OnArmyStrategyChanged().ForEachAsync(armyStrategy =>
			{
				var armySettings = getArmySettings.Invoke(armyId);
				var newArmySettings = armySettings.ChangeArmyStrategy(armyStrategy);
				SaveAndShowArmySettings(armyId, newArmySettings);
			});
		}

		void SaveAndShowArmySettings(int armyId, ArmySettings armySettings)
		{
			saveArmySettings.Invoke(armyId, armySettings);
			ShowArmySettings(armySettings);
		}
	}
}