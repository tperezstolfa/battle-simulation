using Cysharp.Threading.Tasks;
using Exercise.BattleCreation.Core.Actions;
using Exercise.BattleCreation.View;

namespace Exercise.BattleCreation.Presentation
{
	public class LaunchMenuPresenter
	{
		readonly LaunchMenuView view;
		readonly StartBattle startBattle;
		readonly ArmyMenuPresenterFactory armyMenuPresenterFactory;

		public LaunchMenuPresenter(
			LaunchMenuView view,
			StartBattle startBattle,
			ArmyMenuPresenterFactory armyMenuPresenterFactory)
		{
			this.view = view;
			this.startBattle = startBattle;
			this.armyMenuPresenterFactory = armyMenuPresenterFactory;
		}

		public async UniTask Present()
		{
			PresentArmiesSettings();
			await view.OnRequestBattleStart();
			startBattle.Invoke();
		}

		void PresentArmiesSettings()
		{
			PresentArmySettings(1, view.GetArmy1Menu());
			PresentArmySettings(2, view.GetArmy2Menu());
		}

		void PresentArmySettings(int armyId, ArmyMenuView armyMenu)
		{
			var armyMenuPresenter = armyMenuPresenterFactory.Create(armyMenu);
			armyMenuPresenter.Present(armyId);
		}
	}
}