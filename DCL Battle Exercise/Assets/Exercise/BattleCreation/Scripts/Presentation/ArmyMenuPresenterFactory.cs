using Exercise.BattleCreation.Core.Actions;
using Exercise.BattleCreation.View;
using JetBrains.Annotations;

namespace Exercise.BattleCreation.Presentation
{
	public class ArmyMenuPresenterFactory
	{
		readonly GetArmySettings getArmySettings;
		readonly SaveArmySettings saveArmySettings;

		[UsedImplicitly]
		protected ArmyMenuPresenterFactory()
		{
			//NSubstitute needs a parameterless constructor
		}

		public ArmyMenuPresenterFactory(GetArmySettings getArmySettings, SaveArmySettings saveArmySettings)
		{
			this.getArmySettings = getArmySettings;
			this.saveArmySettings = saveArmySettings;
		}

		public virtual ArmyMenuPresenter Create(ArmyMenuView armyMenu)
		{
			return new ArmyMenuPresenter(armyMenu, getArmySettings, saveArmySettings);
		}
	}
}