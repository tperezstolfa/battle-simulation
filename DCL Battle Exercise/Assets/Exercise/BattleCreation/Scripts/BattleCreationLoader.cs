using Exercise.BattleCreation.Core.Actions;
using Exercise.BattleCreation.Core.Domain;
using Exercise.BattleCreation.Infrastructure;
using Exercise.BattleCreation.Presentation;
using Exercise.BattleCreation.View;
using UnityEngine;

namespace Exercise.BattleCreation
{
	public class BattleCreationLoader : MonoBehaviour
	{
		[SerializeField] Canvas canvas;
		[SerializeField] LaunchMenu launchMenuPrefab;
		[SerializeField] BattleSettings armySettingsRepository;

		async void Start()
		{
			var launchMenuView = Instantiate(launchMenuPrefab, canvas.transform);
			var startBattle = new StartBattle();
			var getArmySettings = new GetArmySettings(armySettingsRepository);
			var saveArmySettings = new SaveArmySettings(armySettingsRepository);
			var armyMenuPresenterFactory = new ArmyMenuPresenterFactory(getArmySettings, saveArmySettings);
			var launchMenuPresenter = new LaunchMenuPresenter(launchMenuView, startBattle, armyMenuPresenterFactory);
			await launchMenuPresenter.Present();
		}
	}
}