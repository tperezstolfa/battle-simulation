using Exercise.BattleCreation.Core.Domain;
using Exercise.BattleCreation.Presentation;
using NSubstitute;
using NUnit.Framework;

namespace Exercise.BattleCreation.Core.Actions
{
	public class SaveArmySettingsTest
	{
		SaveArmySettings saveArmySettings;
		ArmySettingsRepository armySettingsRepository;

		[SetUp]
		public void BeforeEach()
		{
			armySettingsRepository = Substitute.For<ArmySettingsRepository>();
			
			saveArmySettings = new SaveArmySettings(armySettingsRepository);
		}

		[Test]
		public void Should_Save_Army_Settings()
		{
			const int ArmyId = 1;
			var validArmySettings = new ArmySettings();

			WhenArmySettingsAreSaved(ArmyId, validArmySettings);

			ThenArmySettingsAreSaved(ArmyId, validArmySettings);
		}

		void WhenArmySettingsAreSaved(int armyId, ArmySettings armySettings)
		{
			saveArmySettings.Invoke(armyId, armySettings);
		}

		void ThenArmySettingsAreSaved(int armyId, ArmySettings armySettings)
		{
			armySettingsRepository.Received(1).Save(armyId, armySettings);
		}
	}
}