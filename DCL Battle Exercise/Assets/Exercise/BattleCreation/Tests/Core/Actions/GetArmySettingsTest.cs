using Exercise.BattleCreation.Core.Domain;
using NSubstitute;
using NUnit.Framework;

namespace Exercise.BattleCreation.Core.Actions
{
	public class GetArmySettingsTest
	{
		GetArmySettings getArmySettings;
		ArmySettingsRepository armySettingsRepository;

		[SetUp]
		public void BeforeEach()
		{
			armySettingsRepository = Substitute.For<ArmySettingsRepository>();
			
			getArmySettings = new GetArmySettings(armySettingsRepository);
		}

		[Test]
		public void Should_Get_Army_Settings()
		{
			const int ArmyId = 1;
			var expectedArmySettings = GivenStoredArmySettings(ArmyId);

			var retrievedArmySettings = WhenArmySettingsAreGet(ArmyId);

			ThenExpectedArmySettingsAreRetrieved(expectedArmySettings, retrievedArmySettings);
		}

		ArmySettings GivenStoredArmySettings(int armyId)
		{
			var validArmySettings = new ArmySettings();

			armySettingsRepository.Get(armyId).Returns(validArmySettings);

			return validArmySettings;
		}

		ArmySettings WhenArmySettingsAreGet(int armyId)
		{
			return getArmySettings.Invoke(armyId);
		}

		void ThenExpectedArmySettingsAreRetrieved(ArmySettings expectedArmySettings, ArmySettings retrievedArmySettings)
		{
			Assert.AreEqual(expectedArmySettings, retrievedArmySettings);
		}
	}
}