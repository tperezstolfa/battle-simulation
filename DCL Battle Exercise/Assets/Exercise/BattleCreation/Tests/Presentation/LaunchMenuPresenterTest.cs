using System;
using System.Collections;
using System.Threading;
using Cysharp.Threading.Tasks;
using Exercise.BattleCreation.Core.Actions;
using Exercise.BattleCreation.View;
using NSubstitute;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace Exercise.BattleCreation.Presentation
{
	public class LaunchMenuPresenterTest
	{
		LaunchMenuPresenter presenter;
		StartBattle startBattle;
		LaunchMenuView view;
		ArmyMenuPresenterFactory armyMenuPresenterFactory;

		[SetUp]
		public void BeforeEach()
		{
			view = Substitute.For<LaunchMenuView>();
			startBattle = Substitute.For<StartBattle>();
			armyMenuPresenterFactory = Substitute.For<ArmyMenuPresenterFactory>();

			presenter = new LaunchMenuPresenter(view, startBattle, armyMenuPresenterFactory);
		}

		[Test]
		public void Should_Not_Start_Battle_When_Not_Requested()
		{
			view.OnRequestBattleStart().Returns(UniTask.Never(CancellationToken.None));

			WhenPresents();

			ThenBattleIsNotStarted();
		}

		[UnityTest]
		public IEnumerator Should_Start_Battle_When_Requested() => UniTask.ToCoroutine(async () =>
		{
			view.OnRequestBattleStart().Returns(UniTask.CompletedTask);

			await WhenPresents();

			ThenBattleIsStarted();
		});

		[UnityTest]
		public IEnumerator  Should_Present_Armies_Menus_When_Presented() => UniTask.ToCoroutine(async () =>
		{
			var army1Menu = GivenAnArmy1Menu();
			var army2Menu = GivenAnArmy2Menu();
			var army1MenuPresenter = GivenArmyMenuWillBeCreated(army1Menu);
			var army2MenuPresenter = GivenArmyMenuWillBeCreated(army2Menu);

			await WhenPresents();
			
			ThenArmyMenusAreRetrieved();
			ThenArmyMenusAreCreated(army1Menu, army2Menu);
			ThenArmyMenusArePresented(army1MenuPresenter, army2MenuPresenter);
		});

		ArmyMenuView GivenAnArmy2Menu()
		{
			var army2Menu = Substitute.For<ArmyMenuView>();
			view.GetArmy2Menu().Returns(army2Menu);
			return army2Menu;
		}

		ArmyMenuView GivenAnArmy1Menu()
		{
			var army1Menu = Substitute.For<ArmyMenuView>();
			view.GetArmy1Menu().Returns(army1Menu);
			return army1Menu;
		}

		ArmyMenuPresenter GivenArmyMenuWillBeCreated(ArmyMenuView armyMenu)
		{
			var armyMenuPresenter = Substitute.For<ArmyMenuPresenter>();
			armyMenuPresenterFactory.Create(armyMenu).Returns(armyMenuPresenter);
			return armyMenuPresenter;
		}

		void ThenArmyMenusAreRetrieved()
		{
			view.Received(1).GetArmy1Menu();
			view.Received(1).GetArmy2Menu();
		}

		void ThenArmyMenusAreCreated(ArmyMenuView army1Menu, ArmyMenuView army2Menu)
		{
			armyMenuPresenterFactory.Received(1).Create(army1Menu);
			armyMenuPresenterFactory.Received(1).Create(army2Menu);
		}

		static void ThenArmyMenusArePresented(ArmyMenuPresenter army1MenuPresenter, ArmyMenuPresenter army2MenuPresenter)
		{
			army1MenuPresenter.Received(1).Present(1);
			army2MenuPresenter.Received(1).Present(2);
		}

		async UniTask WhenPresents()
		{
			await presenter.Present();
		}

		void ThenBattleIsStarted()
		{
			startBattle.Received(1).Invoke();
		}

		void ThenBattleIsNotStarted()
		{
			startBattle.DidNotReceive().Invoke();
		}
	}
}