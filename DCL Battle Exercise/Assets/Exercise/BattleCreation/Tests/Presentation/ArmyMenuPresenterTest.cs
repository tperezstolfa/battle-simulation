using System.Collections;
using Cysharp.Threading.Tasks;
using Cysharp.Threading.Tasks.Linq;
using Exercise.BattleCreation.Core.Actions;
using Exercise.BattleCreation.Core.Domain;
using Exercise.BattleCreation.View;
using NSubstitute;
using NUnit.Framework;
using UnityEngine.TestTools;

namespace Exercise.BattleCreation.Presentation
{
	public class ArmyMenuPresenterTest
	{
		ArmyMenuView view;
		GetArmySettings getArmySettings;
		ArmyMenuPresenter presenter;
		SaveArmySettings saveArmySettings;

		[SetUp]
		public void BeforeEach()
		{
			view = Substitute.For<ArmyMenuView>();
			getArmySettings = Substitute.For<GetArmySettings>();
			saveArmySettings = Substitute.For<SaveArmySettings>();

			presenter = new ArmyMenuPresenter(view, getArmySettings, saveArmySettings);
		}

		[Test]
		public void Should_Set_Army_Settings_When_Presented()
		{
			const int ArmyId = 1;
			var armySettings = GivenArmySettings(ArmyId);

			WhenPresenting(ArmyId);

			ThenShowsArmySettings(armySettings);
		}

		[Test]
		public void Should_Save_Army_Settings_And_Show_Each_Time_Archers_Count_Changes() 
		{
			const int ArmyId = 1;
			const int newArcherCount = 22;
			const int anotherArcherCount = 5;

			var armySettings = GivenArmySettings(ArmyId);
			view.OnArchersCountChanged()
				.Returns(UniTaskAsyncEnumerable.Create<int>(async (writer, token) =>
				{
					await writer.YieldAsync(newArcherCount);
					await writer.YieldAsync(anotherArcherCount);
				}));

			WhenPresenting(ArmyId);

			var firstExpectedSettings = new ArmySettings()
				{archerCount = newArcherCount, warriorCount = armySettings.warriorCount, armyStrategyName = armySettings.armyStrategyName};
			var secondExpectedSettings = new ArmySettings()
				{archerCount = anotherArcherCount, warriorCount = armySettings.warriorCount, armyStrategyName = armySettings.armyStrategyName};
			ThenSavesArmySettings(ArmyId, firstExpectedSettings);
			ThenSavesArmySettings(ArmyId, secondExpectedSettings);
			ThenShowsArmySettings(firstExpectedSettings);
			ThenShowsArmySettings(secondExpectedSettings);
		}
		
		[Test]
		public void Should_Save_Army_Settings_And_Show_Each_Time_Warriors_Count_Changes() 
		{
			const int ArmyId = 1;
			const int newWarriorCount = 22;
			const int anotherWarriorCount = 5;

			var armySettings = GivenArmySettings(ArmyId);
			view.OnWarriorsCountChanged()
				.Returns(UniTaskAsyncEnumerable.Create<int>(async (writer, token) =>
				{
					await writer.YieldAsync(newWarriorCount);
					await writer.YieldAsync(anotherWarriorCount);
				}));

			WhenPresenting(ArmyId);

			var firstExpectedSettings = new ArmySettings()
				{warriorCount = newWarriorCount, archerCount = armySettings.archerCount, armyStrategyName = armySettings.armyStrategyName};
			var secondExpectedSettings = new ArmySettings()
				{warriorCount = anotherWarriorCount, archerCount = armySettings.archerCount, armyStrategyName = armySettings.armyStrategyName};
			ThenSavesArmySettings(ArmyId, firstExpectedSettings);
			ThenSavesArmySettings(ArmyId, secondExpectedSettings);
			ThenShowsArmySettings(firstExpectedSettings);
			ThenShowsArmySettings(secondExpectedSettings);
		}
		
		[Test]
		public void Should_Save_Army_Settings_And_Show_Each_Time_Army_Strategy_Changes() 
		{
			const int ArmyId = 1;
			const ArmyStrategyName newArmyStrategy = ArmyStrategyName.StrategyBasic;
			const ArmyStrategyName anotherArmyStrategy = ArmyStrategyName.StrategyDefensive;

			var armySettings = GivenArmySettings(ArmyId);
			view.OnArmyStrategyChanged()
				.Returns(UniTaskAsyncEnumerable.Create<ArmyStrategyName>(async (writer, token) =>
				{
					await writer.YieldAsync(newArmyStrategy);
					await writer.YieldAsync(anotherArmyStrategy);
				}));

			WhenPresenting(ArmyId);

			var firstExpectedSettings = new ArmySettings()
				{armyStrategyName = newArmyStrategy, archerCount = armySettings.archerCount, warriorCount = armySettings.warriorCount};
			var secondExpectedSettings = new ArmySettings()
				{armyStrategyName = anotherArmyStrategy, archerCount = armySettings.archerCount, warriorCount = armySettings.warriorCount};
			ThenSavesArmySettings(ArmyId, firstExpectedSettings);
			ThenSavesArmySettings(ArmyId, secondExpectedSettings);
			ThenShowsArmySettings(firstExpectedSettings);
			ThenShowsArmySettings(secondExpectedSettings);
		}

		void ThenSavesArmySettings(int armyId, ArmySettings armySettings)
		{
			saveArmySettings.Received(1).Invoke(armyId, armySettings);
		}

		ArmySettings GivenArmySettings(int armyId)
		{
			var validArmySettings = new ArmySettings();

			getArmySettings.Invoke(armyId).Returns(validArmySettings);

			return validArmySettings;
		}

		void WhenPresenting(int armyId)
		{
			presenter.Present(armyId);
		}

		void ThenShowsArmySettings(ArmySettings armySettings)
		{
			view.Received(1).Show(armySettings);
		}
	}
}